# README #

demo.html provides a instructions and examples of how to utilize Lime Light Traffic Attribution on front end traffic sites connecting to LimeLight Platform.

### What is this repository for? ###

* Provides simple library for dealing with Lime Light Traffic Attribution parameters (utm_source, utm_campaign, utm_medium, utm_term, utm_content, and device_category)
* Version 1.0.0

### How do I get set up? ###

* See Demo running live at http://cdn.limelightcrm.com/traffic-attribution/demo.html
* Javascript available on CDN at http://cdn.limelightcrm.com/traffic-attribution/limelight-traffic-attribution.js
* Javascript minified available on CDN at http://cdn.limelightcrm.com/traffic-attribution/limelight-traffic-attribution.min.js

### Contribution guidelines ###

* Submit pull requests for changes
* Utilizes https://github.com/kaimallea/isMobile
* Utilizes https://github.com/medius/utm_form

### Who do I talk to? ###

* operations@limelightcrm.com
